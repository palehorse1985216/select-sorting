function selectSort($list) {
    $length = count($list);
    for ($i=0; $i<$length; $i++) {
        $min = PHP_INT_MAX;
        $min_index = -1;
        for ($j=$i; $j<$length; $j++) {
            if ($list[$j] <= $min) {
                $min = $list[$j];
                $min_index = $j;
            }
        }
        $temp = $list[$i];
        $list[$i] = $list[$min_index];
        $list[$min_index] = $temp;
    }
    return $list;
}